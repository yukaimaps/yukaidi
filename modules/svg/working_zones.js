import { svgDataBuilder } from './data';
import oauth from '../auth';

export function svgWorkingZones(projection, context, dispatch) {
  var layerData = svgDataBuilder('layer-mapdata');
  drawWorkingZones = layerData(projection, context, dispatch);
  drawWorkingZones.showLabels(false);
  oauth.xhr({
    api_url: BACKEND_URL,
    path: '/working-zones/',
    method: 'GET',
  }, (err, response) => {
    if (!err) {
      drawWorkingZones.setFile('.json', response);
    }
  });
  return drawWorkingZones;
}

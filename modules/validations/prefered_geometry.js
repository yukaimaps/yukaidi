import { actionAddEntity } from '../actions/add_entity';
import { actionAddVertex } from '../actions/add_vertex';
import { modeDrawLine } from '../modes/draw_line';
import { operationDelete } from '../operations/delete';
import { modeDrawArea } from '../modes/draw_area';
import { t, localizer } from '../core/localizer';
import { presetManager } from '../presets';
import { validationIssue, validationIssueFix } from '../core/validation';
import { utilDisplayLabel } from '../util';
import { osmNode, osmWay } from '../osm';


// Whitelist of presets allowing preferred geometries.
// If a feature matching one of those presets allow
// multiple geometries, we consider the first one as preffered.
// Entries ending with '/' as supposed to match all sub-presets.
const WHITELIST = [
  // all point of interesets
  'PointOfInterest/',
  // all entrances
  'Entrance/',
  // all obstacles
  'Obstacle/',
  // Some parking bays
  'ParkingBay/Disabled',
  // some quays
  'Quay/Bus',
  'Quay/CableWay',
  'Quay/Coach',
  'Quay/Funicular',
  'Quay/TrolleyBus',
  'Quay/Water',
  'InsideSpace/Quay/Metro',
  'InsideSpace/Quay/Rail',
];


function isPresetWhitelisted(preset) {
  for (const tested of WHITELIST) {
    if (tested === preset.id) {
      return true;
    }
    if (tested.endsWith('/')) {
      if (preset.id.startsWith(tested)) {
        return true;
      }
    }
  }
  return false;
}


const auto_fixes = {
  'point': {
    'line': (context, entity) => [
      new validationIssueFix({
        icon: 'iD-operation-continue',
        title: t.append('issues.fix.continue_to_line.title'),
        onClick: function(context) {
          var node = context.hasEntity(entity.id);
          const tags = node.tags;
          var way = osmWay({ tags: tags });
          context.perform(
            (graph) => graph.replace(node.update({ tags: []})),
            actionAddEntity(way),
            actionAddVertex(way.id, node.id)
          );
          context.enter(modeDrawLine(
            context, way.id, context.graph(), 'line', 'prefix', false
          ));
        }
      })
    ],
    'area': (context, entity) => [
      new validationIssueFix({
        icon: 'iD-operation-continue',
        title: t.append('issues.fix.continue_to_area.title'),
        onClick: function(context) {
          var node = context.hasEntity(entity.id);
          const tags = node.tags;
          var way = osmWay({ tags: tags });
          context.perform(
            (graph) => graph.replace(node.update({ tags: []})),
            actionAddEntity(way),
            actionAddVertex(way.id, node.id)
          );
          context.enter(modeDrawArea(
            context, way.id, context.graph(), 'area'
          ));
        }
      }),
    ],
    'vertex': (context, entity) => [
      new validationIssueFix({
        'icon': 'iD-operation-continue',
        title: t.append('issues.fix.continue_to_vertex.title'),
        onClick: function(context) {
          var node = context.hasEntity(entity.id);
          const tags = node.tags;
          var way = osmWay({ });
          context.perform(
            actionAddEntity(way),
            actionAddVertex(way.id, node.id)
          );
          context.enter(modeDrawArea(
            context, way.id, context.graph(), 'line', 'prefix', false
          ));
        }
      }),
    ]
  },
};




export function validationPreferredGeometry() {
  var type = 'preferred_geometry';

  function showReference(selection) {
    selection.selectAll('.issue-reference')
      .data([0])
      .enter()
      .append('div')
      .attr('class', 'issue-reference')
      .call(t.append('issues.preferred_geometry.reference'));
  }

  function hasPreferredGeometry(entity, graph) {
    const presets = presetManager;
    const geometry = entity.geometry(graph);
    const preset = presets.matchTags(entity.tags, geometry);
    if (isPresetWhitelisted(preset)) {
      const preferredGeom = preset.geometry[0];
      if (preferredGeom !== geometry) {
        return preferredGeom;
      }
    }
    return null;
  }

  var validation = function (entity, graph) {
    const pref = hasPreferredGeometry(entity, graph);
    const presets = presetManager;
    const geom = entity.geometry(graph);
    if (pref) {
      return [new validationIssue({
        issue: type,
        severity: 'warning',
        message: function(context) {
          const name = utilDisplayLabel(entity, context.graph());
          return t.append(`issues.preferred_as_${pref}.message`, {
            name: name,
            preferred: pref,
          });
        },
        reference: showReference,
        entityIds: [entity.id],
        dynamicFixes: function(context) {
          const makeFixes = (auto_fixes[geom] || {})[pref];
          const fixes = makeFixes ? makeFixes(context, entity) : [];
          fixes.push(new validationIssueFix({
            icon: 'iD-operation-delete',
            title: t.append('issues.fix.delete_feature.title'),
            entityIds: [entity.id],
            onClick: function(context) {
              var id = this.issue.entityIds[0];
              var operation = operationDelete(context, [id]);
              if (!operation.disabled()) {
                operation();
              }
            }
          }));
          return fixes;
        }
      })];
    }
    return [];
  };

  validation.type = type;

  return validation;
}

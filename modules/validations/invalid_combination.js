import { t } from '../core/localizer';
import { utilDisplayLabel } from '../util';
import { validationIssue, validationIssueFix } from '../core/validation';
import { geoPointInPolygon } from '../geo';


export function validationInvalidCombination(context) {
  const type = 'invalid_combination';
  const incompatibleTags = [
    { key: "Amenity",         ref: "staff",      combination: { "Staffing": "No", "AccessibilityTrainedStaff": "Yes" } },
    { key: "StopPlace",       ref: "audio_info", combination: { "AudibleSignals": "Yes", "AudioInformation": "No" } },
    { key: "StopPlace",       ref: "audio_info", combination: { "AudibleSignals": "No", "AudioInformation": "Yes" } },
    { key: "StopPlace",       ref: "audio_info", combination: { "AudibleSignals": "No", "AudioInformation": "Bad" } },
    { key: "PointOfInterest", ref: "toilets",    has: { "Toilets": "No" },      containsEntityWith: { "Amenity": "Toilets" } },
    { key: "PointOfInterest", ref: "assistance", has: { "Assistance": "None" }, containsEntityWith: { "Amenity": "ReceptionDesk" } },
  ];


  const validation = function checkInvalidCombination(entity, graph) {
    if (!entity.tags) return [];
    const entityID = entity.id;

    function hasTags(entity, tags) {
      return Object.keys(tags).filter(t => entity.tags[t] === tags[t]).length === Object.keys(tags).length;
    }

    function hasConflictingEntitiesInside(rule, outerEntity) {
      var intersected = context.history().tree().intersects(outerEntity.extent(graph), graph);
      var hasConflict = intersected.find(innerEntity => {
        if(!hasTags(innerEntity, rule.containsEntityWith)) { return false; }

        try {
          let outerCoords = outerEntity.asGeoJSON(graph).coordinates;
          while(outerCoords.length === 1) { outerCoords = outerCoords[0]; }
          return geoPointInPolygon(innerEntity.loc, outerCoords);
        }
        catch(e) {
          console.warn(e);
          return false;
        }
      });

      return hasConflict !== undefined;
    }

    return incompatibleTags.filter(it => {
      // Check for conflicts in tags of a single entity
      if(
        it.combination
        && entity.tags[it.key]
        && hasTags(entity, it.combination)
      ) {
        return it;
      }

      // Check for conflicts between an entity and entities inside of it
      else if(
        it.containsEntityWith
        && entity.tags[it.key]
        && hasTags(entity, it.has)
        && entity.geometry(graph) == 'area'
        && hasConflictingEntitiesInside(it, entity)
      ) {
        return it;
      }

      // No problem
      else { return false; }
    })
    .map(it => (new validationIssue({
      type: type,
      severity: 'warning',
      message: (context) => {
        const entity = context.hasEntity(entityID);
        return entity ? t.append('issues.invalid_combination.message', {
          feature: utilDisplayLabel(entity, context.graph(), true /* verbose */),
          value: it.ref
        }) : '';
      },
      reference: getReference(it.ref),
      entityIds: [entityID],
      hash: it.ref,
    })));


    function getReference(id) {
      return function showReference(selection) {
        selection.selectAll('.issue-reference')
          .data([0])
          .enter()
          .append('div')
          .attr('class', 'issue-reference')
          .call(t.append(`issues.invalid_combination.reference.${id}`));
      };
    }
  };

  validation.type = type;

  return validation;
}

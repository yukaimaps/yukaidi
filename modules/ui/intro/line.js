import { dispatch as d3_dispatch } from 'd3-dispatch';

import {
    select as d3_select
} from 'd3-selection';

import { presetManager } from '../../presets';
import { t } from '../../core/localizer';
import { geoSphericalDistance } from '../../geo';
import { actionChangePreset } from '../../actions/change_preset';
import { modeBrowse } from '../../modes/browse';
import { modeSelect } from '../../modes/select';
import { utilRebind } from '../../util/rebind';
import { helpHtml, pointBox, icon, pad, selectMenuItem, transitionTime } from './helper';


export function uiIntroLine(context, reveal) {
    var dispatch = d3_dispatch('done');
    var timeouts = [];
    var _tulipRoadID = null;
    var flowerRoadID = 'w14';
    var tulipRoadStart = [2.6492876, 48.838157];
    var tulipRoadMidpoint = [2.6492517, 48.8383799];
    var tulipRoadIntersection = [2.6492158, 48.8386024];
    var roadCategory = presetManager.item('category-path');
    var residentialPreset = presetManager.item('SitePathLink/Pavement');


    var chapter = {
        title: 'intro.lines.title'
    };


    function timeout(f, t) {
        timeouts.push(window.setTimeout(f, t));
    }


    function eventCancel(d3_event) {
        d3_event.stopPropagation();
        d3_event.preventDefault();
    }


    function addLine() {
        context.enter(modeBrowse(context));
        context.history().reset('initial');

        var msec = transitionTime(tulipRoadStart, context.map().center());
        if (msec) { reveal(null, null, { duration: 0 }); }
        context.map().centerZoomEase(tulipRoadStart, 20, msec);

        timeout(function() {
            var tooltip = reveal('button.add-line',
                helpHtml('intro.lines.add_line'));

            tooltip.selectAll('.popover-inner')
                .insert('svg', 'span')
                .attr('class', 'tooltip-illustration')
                .append('use')
                .attr('xlink:href', '#iD-graphic-lines');

            context.on('enter.intro', function(mode) {
                if (mode.id !== 'add-line') return;
                continueTo(startLine);
            });
        }, msec + 100);

        function continueTo(nextStep) {
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function startLine() {
        if (context.mode().id !== 'add-line') return chapter.restart();

        _tulipRoadID = null;

        var padding = 70 * Math.pow(2, context.map().zoom() - 20);
        var box = pad(tulipRoadStart, padding, context);

        var textId = context.lastPointerType() === 'mouse' ? 'start_line' : 'start_line_tap';
        var startLineString = helpHtml('intro.lines.missing_road') + '{br}' +
            helpHtml('intro.lines.line_draw_info') +
            helpHtml('intro.lines.' + textId);
        reveal(box, startLineString);

        context.map().on('move.intro drawn.intro', function() {
            padding = 70 * Math.pow(2, context.map().zoom() - 20);
            box = pad(tulipRoadStart, padding, context);
            reveal(box, startLineString, { duration: 0 });
        });

        context.on('enter.intro', function(mode) {
            if (mode.id !== 'draw-line') return chapter.restart();
            continueTo(drawLine);
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function drawLine() {
        if (context.mode().id !== 'draw-line') return chapter.restart();

        _tulipRoadID = context.mode().selectedIDs()[0];
        context.map().centerZoomEase(tulipRoadMidpoint, 19, 500);

        timeout(function() {
            var padding = 200 * Math.pow(2, context.map().zoom() - 18.5);
            var box = pad(tulipRoadMidpoint, padding, context);
            box.height = box.height * 2;
            reveal(box,
                helpHtml('intro.lines.intersect')
            );

            context.map().on('move.intro drawn.intro', function() {
                padding = 200 * Math.pow(2, context.map().zoom() - 18.5);
                box = pad(tulipRoadMidpoint, padding, context);
                box.height = box.height * 2;
                reveal(box,
                    helpHtml('intro.lines.intersect'),
                    { duration: 0 }
                );
            });
        }, 550);  // after easing..

        context.history().on('change.intro', function() {
            if (isLineConnected()) {
                continueTo(continueLine);
            }
        });

        context.on('enter.intro', function(mode) {
            if (mode.id === 'draw-line') {
                return;
            } else if (mode.id === 'select') {
                continueTo(retryIntersect);
                return;
            } else {
                return chapter.restart();
            }
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.history().on('change.intro', null);
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function isLineConnected() {
        var entity = _tulipRoadID && context.hasEntity(_tulipRoadID);
        if (!entity) return false;

        var drawNodes = context.graph().childNodes(entity);
        return drawNodes.some(function(node) {
            return context.graph().parentWays(node).some(function(parent) {
                return parent.id === flowerRoadID;
            });
        });
    }


    function retryIntersect() {
        d3_select(window).on('pointerdown.intro mousedown.intro', eventCancel, true);

        var box = pad(tulipRoadIntersection, 80, context);
        reveal(box,
            helpHtml('intro.lines.retry_intersect')
        );

        timeout(chapter.restart, 3000);
    }


    function continueLine() {
        if (context.mode().id !== 'draw-line') return chapter.restart();
        var entity = _tulipRoadID && context.hasEntity(_tulipRoadID);
        if (!entity) return chapter.restart();

        var continueLineText = helpHtml('intro.lines.finish_line_' + (context.lastPointerType() === 'mouse' ? 'click' : 'tap')) +
            helpHtml('intro.lines.finish_road');

        reveal('.surface', continueLineText);

        context.on('enter.intro', function(mode) {
            if (mode.id === 'draw-line') {
                return;
            } else if (mode.id === 'select') {
                return continueTo(chooseCategoryRoad);
            } else {
                return chapter.restart();
            }
        });

        function continueTo(nextStep) {
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function chooseCategoryRoad() {
        if (context.mode().id !== 'select') return chapter.restart();

        context.on('exit.intro', function() {
            return chapter.restart();
        });

        var button = context.container().select('.preset-category-path .preset-list-button');
        if (button.empty()) return chapter.restart();

        // disallow scrolling
        context.container().select('.inspector-wrap').on('wheel.intro', eventCancel);

        timeout(function() {
            // reset pane, in case user somehow happened to change it..
            context.container().select('.inspector-wrap .panewrap').style('right', '-100%');

            reveal(button.node(),
                helpHtml('intro.lines.choose_category_road', { category: roadCategory.name() })
            );

            button.on('click.intro', function() {
                continueTo(choosePresetResidential);
            });

        }, 400);  // after editor pane visible

        function continueTo(nextStep) {
            context.container().select('.inspector-wrap').on('wheel.intro', null);
            context.container().select('.preset-list-button').on('click.intro', null);
            context.on('exit.intro', null);
            nextStep();
        }
    }


    function choosePresetResidential() {
        if (context.mode().id !== 'select') return chapter.restart();

        context.on('exit.intro', function() {
            return chapter.restart();
        });

        var subgrid = context.container().select('.preset-category-path .subgrid');
        if (subgrid.empty()) return chapter.restart();

        subgrid.selectAll(':not(.preset-SitePathLink-Pavement) .preset-list-button')
            .on('click.intro', function() {
                continueTo(retryPresetResidential);
            });

        subgrid.selectAll('.preset-SitePathLink-Pavement .preset-list-button')
            .on('click.intro', function() {
                continueTo(nameRoad);
            });

        timeout(function() {
            reveal(subgrid.node(),
                helpHtml('intro.lines.choose_preset_sidewalk', { preset: residentialPreset.name() }),
                { tooltipBox: '.preset-SitePathLink-Pavement .preset-list-button', duration: 300 }
            );
        }, 300);

        function continueTo(nextStep) {
            context.container().select('.preset-list-button').on('click.intro', null);
            context.on('exit.intro', null);
            nextStep();
        }
    }


    // selected wrong road type
    function retryPresetResidential() {
        if (context.mode().id !== 'select') return chapter.restart();

        context.on('exit.intro', function() {
            return chapter.restart();
        });

        // disallow scrolling
        context.container().select('.inspector-wrap').on('wheel.intro', eventCancel);

        timeout(function() {
            var button = context.container().select('.entity-editor-pane .preset-list-button');

            reveal(button.node(),
                helpHtml('intro.lines.retry_preset_sidewalk', { preset: residentialPreset.name() })
            );

            button.on('click.intro', function() {
                continueTo(chooseCategoryRoad);
            });

        }, 500);

        function continueTo(nextStep) {
            context.container().select('.inspector-wrap').on('wheel.intro', null);
            context.container().select('.preset-list-button').on('click.intro', null);
            context.on('exit.intro', null);
            nextStep();
        }
    }


    function nameRoad() {
        context.on('exit.intro', function() {
            continueTo(didNameRoad);
        });

        timeout(function() {
            reveal('.entity-editor-pane',
                helpHtml('intro.lines.name_road', { button: { html: icon('#iD-icon-close', 'inline') } }),
                { tooltipClass: 'intro-lines-name_road' }
            );
        }, 500);

        function continueTo(nextStep) {
            context.on('exit.intro', null);
            nextStep();
        }
    }


    function didNameRoad() {
        context.history().checkpoint('doneAddLine');

        timeout(function() {
            reveal('.surface', helpHtml('intro.lines.did_name_road'), {
                buttonText: t.html('intro.ok'),
                buttonCallback: function() { continueTo(rightClickRoad); }
            });
        }, 500);

        function continueTo(nextStep) {
            nextStep();
        }
    }


    function rightClickRoad() {
        if (!_tulipRoadID) return chapter.restart();
        var entity = context.hasEntity(_tulipRoadID);
        if (!entity) return chapter.restart();

        context.enter(modeBrowse(context));

        var box = pointBox(tulipRoadMidpoint, context);
        var textId = context.lastPointerType() === 'mouse' ? 'rightclick' : 'edit_menu_touch';
        reveal(box, helpHtml('intro.lines.' + textId), { duration: 600 });

        timeout(function() {
            context.map().on('move.intro', function() {
                var entity = context.hasEntity(_tulipRoadID);
                if (!entity) return chapter.restart();
                var box = pointBox(tulipRoadMidpoint, context);
                box.height = box.height * 2;
                reveal(box, helpHtml('intro.lines.' + textId), { duration: 0 });
            });
        }, 600); // after reveal

        context.on('enter.intro', function(mode) {
            if (mode.id !== 'select') return;
            var ids = context.selectedIDs();
            if (ids.length !== 1 || ids[0] !== _tulipRoadID) return;

            timeout(function() {
                var node = selectMenuItem(context, 'delete').node();
                if (!node) return;
                continueTo(enterDelete);
            }, 50);  // after menu visible
        });

        function continueTo(nextStep) {
            context.on('enter.intro', null);
            context.map().on('move.intro', null);
            nextStep();
        }
    }


    function enterDelete() {
        if (!_tulipRoadID) return chapter.restart();
        var entity = context.hasEntity(_tulipRoadID);
        if (!entity) return chapter.restart();

        var node = selectMenuItem(context, 'delete').node();
        if (!node) { return continueTo(rightClickRoad); }

        reveal('.edit-menu',
            helpHtml('intro.lines.delete'),
            { padding: 50 }
        );

        timeout(function() {
            context.map().on('move.intro', function() {
                reveal('.edit-menu',
                    helpHtml('intro.lines.delete'),
                    { duration: 0,  padding: 50 }
                );
            });
        }, 300); // after menu visible

        context.on('exit.intro', function() {
            if (!_tulipRoadID) return chapter.restart();
            var entity = context.hasEntity(_tulipRoadID);
            if (entity) return continueTo(rightClickRoad);  // point still exists
        });

        context.history().on('change.intro', function(changed) {
            if (changed.deleted().length) {
                continueTo(undo);
            }
        });

        function continueTo(nextStep) {
            context.map().on('move.intro', null);
            context.history().on('change.intro', null);
            context.on('exit.intro', null);
            nextStep();
        }
    }


    function undo() {
        context.history().on('change.intro', function() {
            continueTo(play);
        });

        reveal('.top-toolbar button.undo-button',
            helpHtml('intro.lines.undo')
        );

        function continueTo(nextStep) {
            context.history().on('change.intro', null);
            nextStep();
        }
    }


    function play() {
        dispatch.call('done');
        reveal('.ideditor',
            helpHtml('intro.lines.play', { next: t('intro.areas.title') }), {
                tooltipBox: '.intro-nav-wrap .chapter-area',
                buttonText: t.html('intro.ok'),
                buttonCallback: function() { reveal('.ideditor'); }
            }
        );
    }


    chapter.enter = function() {
        addLine();
    };


    chapter.exit = function() {
        timeouts.forEach(window.clearTimeout);
        d3_select(window).on('pointerdown.intro mousedown.intro', null, true);
        context.on('enter.intro exit.intro', null);
        context.map().on('move.intro drawn.intro', null);
        context.history().on('change.intro', null);
        context.container().select('.inspector-wrap').on('wheel.intro', null);
        context.container().select('.preset-list-button').on('click.intro', null);
    };


    chapter.restart = function() {
        chapter.exit();
        chapter.enter();
    };


    return utilRebind(chapter, dispatch, 'on');
}

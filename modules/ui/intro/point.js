import { dispatch as d3_dispatch } from 'd3-dispatch';
import {
    select as d3_select
} from 'd3-selection';

import { presetManager } from '../../presets';
import { t } from '../../core/localizer';
import { actionChangePreset } from '../../actions/change_preset';
import { modeBrowse } from '../../modes/browse';
import { modeSelect } from '../../modes/select';
import { utilRebind } from '../../util/rebind';
import { helpHtml, icon, pointBox, pad, selectMenuItem, transitionTime } from './helper';


export function uiIntroPoint(context, reveal) {
    var dispatch = d3_dispatch('done');
    var timeouts = [];
    var intersection = [2.6538695, 48.8381248];
    var buildingId = 'w-105622';
    var buildingEntrance = [2.6540485, 48.8380641];
    var connectingPathMidpoint = [2.6540887, 48.838112];
    var connectingPathEndPoint = [2.6541373, 48.8381509];
    var _connectingPathId = null;
    var roadCategory = presetManager.item('category-path');
    var entrancePreset = presetManager.item('Entrance/PointOfInterest');
    var footpathPreset = presetManager.item('SitePathLink/Footpath');
    var _pointID = null;
    var footpathId = "w-105668";


    var chapter = {
        title: 'intro.points.title'
    };


    function timeout(f, t) {
        timeouts.push(window.setTimeout(f, t));
    }


    function eventCancel(d3_event) {
        d3_event.stopPropagation();
        d3_event.preventDefault();
    }


    function addPoint() {
        context.enter(modeBrowse(context));
        context.history().reset('initial');

        var msec = transitionTime(intersection, context.map().center());
        if (msec) { reveal(null, null, { duration: 0 }); }
        context.map().centerZoomEase(intersection, 20, msec);

        timeout(function() {
            var tooltip = reveal('button.add-point',
                helpHtml('intro.points.points_info') + '{br}' + helpHtml('intro.points.add_point'));

            _pointID = null;

            tooltip.selectAll('.popover-inner')
                .insert('svg', 'span')
                .attr('class', 'tooltip-illustration')
                .append('use')
                .attr('xlink:href', '#iD-graphic-points');

            context.on('enter.intro', function(mode) {
                if (mode.id !== 'add-point') return;
                continueTo(placePoint);
            });
        }, msec + 100);

        function continueTo(nextStep) {
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function placePoint() {
        if (context.mode().id !== 'add-point') {
            return chapter.restart();
        }

        var pointBox = pad(buildingEntrance, 100, context);
        var textId = context.lastPointerType() === 'mouse' ? 'add_entrance' : 'add_entrance_touch';
        reveal(pointBox, helpHtml('intro.points.' + textId));

        context.map().on('move.intro drawn.intro', function() {
            pointBox = pad(buildingEntrance, 100, context);
            reveal(pointBox, helpHtml('intro.points.' + textId), { duration: 0 });
        });

        context.on('enter.intro', function(mode) {
            if (mode.id === 'draw-node') {
                return;
            } else if (mode.id === 'select') {
                _pointID = context.mode().selectedIDs()[0];

                if(isEntranceOnBuilding()) {
                    continueTo(searchPreset);
                }
                else {
                    continueTo(retryPlacePoint);
                }
                return;
            } else {
                return chapter.restart();
            }
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.history().on('change.intro', null);
            context.on('enter.intro', null);
            nextStep();
        }
    }

    function isEntranceOnBuilding() {
        var entity = buildingId && context.hasEntity(buildingId);
        if (!entity) return false;

        var drawNodes = context.graph().childNodes(entity);
        return drawNodes.some(function(node) {
            return node.id == _pointID;
        });
    }


    function retryPlacePoint() {
        if (context.mode().id !== 'select') return chapter.restart();

        var pointBox = pad(buildingEntrance, 100, context);
        reveal(pointBox,
            helpHtml('intro.points.retry_place_point')
        );

        timeout(chapter.restart, 3000);
    }


    function searchPreset() {
        if (context.mode().id !== 'select' || !_pointID || !context.hasEntity(_pointID)) {
            return addPoint();
        }

        // disallow scrolling
        context.container().select('.inspector-wrap').on('wheel.intro', eventCancel);

        context.container().select('.preset-search-input')
            .on('keydown.intro', null)
            .on('keyup.intro', checkPresetSearch);

        reveal('.preset-search-input',
            helpHtml('intro.points.search_entrance', { preset: entrancePreset.name() })
        );

        context.on('enter.intro', function(mode) {
            if (!_pointID || !context.hasEntity(_pointID)) {
                return continueTo(addPoint);
            }

            var ids = context.selectedIDs();
            if (mode.id !== 'select' || !ids.length || ids[0] !== _pointID) {
                // keep the user's point selected..
                context.enter(modeSelect(context, [_pointID]));

                // disallow scrolling
                context.container().select('.inspector-wrap').on('wheel.intro', eventCancel);

                context.container().select('.preset-search-input')
                    .on('keydown.intro', null)
                    .on('keyup.intro', checkPresetSearch);

                reveal('.preset-search-input',
                    helpHtml('intro.points.search_entrance', { preset: entrancePreset.name() })
                );

                context.history().on('change.intro', null);
            }
        });


        function checkPresetSearch() {
            var first = context.container().select('.preset-list-item:first-child');

            if (first.classed('preset-Entrance-PointOfInterest')) {
                context.container().select('.preset-search-input')
                    .on('keydown.intro', eventCancel, true)
                    .on('keyup.intro', null);

                reveal(first.select('.preset-list-button').node(),
                    helpHtml('intro.points.choose_entrance', { preset: entrancePreset.name() }),
                    { duration: 300 }
                );

                context.history().on('change.intro', function() {
                    continueTo(addCloseEditor);
                });
            }
        }

        function continueTo(nextStep) {
            context.on('enter.intro', null);
            context.history().on('change.intro', null);
            context.container().select('.inspector-wrap').on('wheel.intro', null);
            context.container().select('.preset-search-input').on('keydown.intro keyup.intro', null);
            nextStep();
        }
    }


    function aboutFeatureEditor() {
        if (context.mode().id !== 'select' || !_pointID || !context.hasEntity(_pointID)) {
            return addPoint();
        }

        timeout(function() {
            reveal('.entity-editor-pane',
                helpHtml('intro.points.feature_editor_close', {button: { html: icon('#iD-icon-close', 'inline') }}),
                {
                    tooltipClass: 'intro-points-describe',
                    buttonText: t.html('intro.ok'),
                    buttonCallback: function() { continueTo(addCloseEditor); }
                }
            );
        }, 400);

        context.on('exit.intro', function() {
            // if user leaves select mode here, just continue with the tutorial.
            continueTo(addCloseEditor);
        });

        function continueTo(nextStep) {
            context.on('exit.intro', null);
            nextStep();
        }
    }


    function addCloseEditor() {
        if (context.mode().id !== 'select' || !_pointID || !context.hasEntity(_pointID)) {
            return addPoint();
        }

        timeout(function() {
            reveal('.entity-editor-pane',
                helpHtml('intro.points.feature_editor_close', {button: { html: icon('#iD-icon-close', 'inline') }}),
                {
                    tooltipClass: 'intro-points-describe',
                    buttonText: t.html('intro.ok'),
                    buttonCallback: function() { continueTo(showMissingPath); }
                }
            );
        }, 400);

        // reset pane, in case user happened to change it..
        context.container().select('.inspector-wrap .panewrap').style('right', '0%');

        context.on('exit.intro', function() {
            continueTo(showMissingPath);
        });

        function continueTo(nextStep) {
            context.on('exit.intro', null);
            nextStep();
        }
    }


    function showMissingPath() {
        context.history().checkpoint('doneAddEntrance');
        context.enter(modeBrowse(context));

        var msec = transitionTime(connectingPathMidpoint, context.map().center());
        if (msec) { reveal(null, null, { duration: 0 }); }
        context.map().centerZoomEase(connectingPathMidpoint, 20, msec);

        var pointBox = pad(connectingPathMidpoint, 100, context);
        var textId = context.lastPointerType() === 'mouse' ? 'add_entrance' : 'add_entrance_touch';
        reveal(pointBox, helpHtml('intro.points.' + textId));

        context.map().on('move.intro drawn.intro', function() {
            pointBox = pad(connectingPathMidpoint, 100, context);
            reveal(pointBox, helpHtml('intro.points.' + textId), { duration: 0 });
            reveal(pointBox,
                helpHtml('intro.points.missing_line'),
                { buttonText: t.html('intro.ok'), buttonCallback: addLine }
            );
        });
    }


    function addLine() {
        timeout(function() {
            var tooltip = reveal('button.add-line',
                helpHtml('intro.points.add_line'));

            context.on('enter.intro', function(mode) {
                if (mode.id !== 'add-line') return;
                continueTo(startLine);
            });
        }, 100);

        function continueTo(nextStep) {
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function startLine() {
        if (context.mode().id !== 'add-line') return chapter.restart();

        _connectingPathId = null;

        var padding = 70 * Math.pow(2, context.map().zoom() - 20);
        var box = pad(buildingEntrance, padding, context);

        var textId = context.lastPointerType() === 'mouse' ? 'start_line' : 'start_line_tap';
        var startLineString = helpHtml('intro.points.' + textId);
        reveal(box, startLineString);

        context.map().on('move.intro drawn.intro', function() {
            padding = 70 * Math.pow(2, context.map().zoom() - 20);
            box = pad(buildingEntrance, padding, context);
            reveal(box, startLineString, { duration: 0 });
        });

        context.on('enter.intro', function(mode) {
            if (mode.id !== 'draw-line') return chapter.restart();
            continueTo(endLine);
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function endLine() {
        if (context.mode().id !== 'draw-line') return chapter.restart();

        _connectingPathId = context.mode().selectedIDs()[0];

        var padding = 70 * Math.pow(2, context.map().zoom() - 20);
        var box = pad(connectingPathEndPoint, padding, context);
        var endLineString = helpHtml('intro.points.intersect');
        reveal(box, endLineString);

        context.map().on('move.intro drawn.intro', function() {
            padding = 70 * Math.pow(2, context.map().zoom() - 20);
            box = pad(connectingPathEndPoint, padding, context);
            reveal(box, endLineString, { duration: 0 });
        });

        context.on('enter.intro', function(mode) {
            if (mode.id !== 'draw-line') return chapter.restart();
            if(isLineConnected()) {
                continueTo(continueLine);
            }
            else {
                continueTo(retryIntersect);
            }
        });

        function continueTo(nextStep) {
            context.map().on('move.intro drawn.intro', null);
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function isLineConnected() {
        var entity = _connectingPathId && context.hasEntity(_connectingPathId);
        if (!entity) return false;

        var drawNodes = context.graph().childNodes(entity);
        return drawNodes.some(function(node) {
            return context.graph().parentWays(node).some(function(parent) {
                return parent.id === footpathId;
            });
        }) && drawNodes.some(function(node) {
            return context.graph().parentWays(node).some(function(parent) {
                return parent.id === buildingId;
            });
        });
    }


    function retryIntersect() {
        var box = pad(connectingPathMidpoint, 80, context);
        reveal(box,
            helpHtml('intro.points.retry_intersect')
        );

        timeout(() => {
            context.history().reset('doneAddEntrance');
            showMissingPath();
        }, 3000);
    }


    function continueLine() {
        if (context.mode().id !== 'draw-line') return chapter.restart();
        var entity = _connectingPathId && context.hasEntity(_connectingPathId);
        if (!entity) return chapter.restart();

        context.map().centerEase(connectingPathMidpoint, 500);

        var continueLineText = helpHtml('intro.points.finish_line_' + (context.lastPointerType() === 'mouse' ? 'click' : 'tap')) +
            helpHtml('intro.points.finish_road');

        reveal('.surface', continueLineText);

        context.on('enter.intro', function(mode) {
            if (mode.id === 'draw-line') {
                return;
            } else if (mode.id === 'select') {
                return continueTo(chooseCategoryRoad);
            } else {
                return chapter.restart();
            }
        });

        function continueTo(nextStep) {
            context.on('enter.intro', null);
            nextStep();
        }
    }


    function chooseCategoryRoad() {
        if (context.mode().id !== 'select') return chapter.restart();

        context.on('exit.intro', function() {
            return chapter.restart();
        });

        var button = context.container().select('.preset-category-path .preset-list-button');
        if (button.empty()) return chapter.restart();

        // disallow scrolling
        context.container().select('.inspector-wrap').on('wheel.intro', eventCancel);

        timeout(function() {
            // reset pane, in case user somehow happened to change it..
            context.container().select('.inspector-wrap .panewrap').style('right', '-100%');

            reveal(button.node(),
                helpHtml('intro.points.choose_category_road', { category: roadCategory.name() })
            );

            button.on('click.intro', function() {
                continueTo(choosePresetFootpath);
            });

        }, 400);  // after editor pane visible

        function continueTo(nextStep) {
            context.container().select('.inspector-wrap').on('wheel.intro', null);
            context.container().select('.preset-list-button').on('click.intro', null);
            context.on('exit.intro', null);
            nextStep();
        }
    }


    function choosePresetFootpath() {
        if (context.mode().id !== 'select') return chapter.restart();

        context.on('exit.intro', function() {
            return chapter.restart();
        });

        var subgrid = context.container().select('.preset-category-path .subgrid');
        if (subgrid.empty()) return chapter.restart();

        subgrid.selectAll(':not(.preset-SitePathLink-Footpath) .preset-list-button')
            .on('click.intro', function() {
                continueTo(retryPresetFootpath);
            });

        subgrid.selectAll('.preset-SitePathLink-Footpath .preset-list-button')
            .on('click.intro', function() {
                continueTo(lineCloseEditor);
            });

        timeout(function() {
            reveal(subgrid.node(),
                helpHtml('intro.points.choose_preset_footpath', { preset: footpathPreset.name() }),
                { tooltipBox: '.preset-SitePathLink-Footpath .preset-list-button', duration: 300 }
            );
        }, 300);

        function continueTo(nextStep) {
            context.container().select('.preset-list-button').on('click.intro', null);
            context.on('exit.intro', null);
            nextStep();
        }
    }


    function retryPresetFootpath() {
        if (context.mode().id !== 'select') return chapter.restart();

        context.on('exit.intro', function() {
            return chapter.restart();
        });

        // disallow scrolling
        context.container().select('.inspector-wrap').on('wheel.intro', eventCancel);

        timeout(function() {
            var button = context.container().select('.entity-editor-pane .preset-list-button');

            reveal(button.node(),
                helpHtml('intro.points.retry_preset_footpath', { preset: footpathPreset.name() })
            );

            button.on('click.intro', function() {
                continueTo(chooseCategoryRoad);
            });

        }, 500);

        function continueTo(nextStep) {
            context.container().select('.inspector-wrap').on('wheel.intro', null);
            context.container().select('.preset-list-button').on('click.intro', null);
            context.on('exit.intro', null);
            nextStep();
        }
    }


    function lineCloseEditor() {
        if (context.mode().id !== 'select' || !_connectingPathId || !context.hasEntity(_connectingPathId)) {
            return chapter.restart();
        }

        timeout(function() {
            reveal('.entity-editor-pane',
                helpHtml('intro.points.line_editor_close', {button: { html: icon('#iD-icon-close', 'inline') }}),
                {
                    tooltipClass: 'intro-points-describe',
                    buttonText: t.html('intro.ok'),
                    buttonCallback: function() { continueTo(play); }
                }
            );
        }, 400);

        // reset pane, in case user happened to change it..
        context.container().select('.inspector-wrap .panewrap').style('right', '0%');

        context.on('exit.intro', function() {
            continueTo(play);
        });

        function continueTo(nextStep) {
            context.on('exit.intro', null);
            nextStep();
        }
    }


    function play() {
        dispatch.call('done');
        reveal('.ideditor',
            helpHtml('intro.points.play', { next: t('intro.tips.title') }), {
                tooltipBox: '.intro-nav-wrap .chapter-tips',
                buttonText: t.html('intro.ok'),
                buttonCallback: function() { reveal('.ideditor'); }
            }
        );
    }


    chapter.enter = function() {
        addPoint();
    };


    chapter.exit = function() {
        timeouts.forEach(window.clearTimeout);
        context.on('enter.intro exit.intro', null);
        context.map().on('move.intro drawn.intro', null);
        context.history().on('change.intro', null);
        context.container().select('.inspector-wrap').on('wheel.intro', eventCancel);
        context.container().select('.preset-search-input').on('keydown.intro keyup.intro', null);
    };


    chapter.restart = function() {
        chapter.exit();
        chapter.enter();
    };


    return utilRebind(chapter, dispatch, 'on');
}

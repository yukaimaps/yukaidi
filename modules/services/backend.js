import oauth from '../auth';

export class Backend {

  constructor() {
    this.api_url = BACKEND_URL;
    this.auth = oauth;
  }

  async uploadImage(file, callback) {
    var data = new FormData();
    data.append('fileb', file);
    await this.auth.xhr({
      method: 'POST',
      api_url: this.api_url,
      path: '/photos/upload',
      content: data,
      headers: { 'Content-Type': 'multipart/form-data' },
    }, (err, response) => {
      if (response) {
        callback(this.api_url + '/photos' + JSON.parse(response).relative_url);
      }
    });
  }
}


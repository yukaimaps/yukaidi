import esbuild from 'esbuild';
import fs from 'node:fs';
import parse from 'minimist';

let args = parse(process.argv.slice(2), {boolean: true});
delete args._;

esbuild
  .build(Object.assign({
    bundle: true,
    sourcemap: true,
    entryPoints: ['./modules/id.js'],
    legalComments: 'none',
    logLevel: 'info',
    metafile: true,
    outfile: 'dist/iD.js',
    define: {
      DEPLOY_URL: JSON.stringify(process.env.DEPLOY_URL || 'https://id.openstreetmap.fr/'),
      API_ROOT: JSON.stringify(process.env.API_ROOT || 'https://www.openstreetmap.org'),
      OIDC_SERVER: JSON.stringify(process.env.OIDC_SERVER || ''),
      OAUTH_CLIENT_ID: JSON.stringify(process.env.OAUTH_CLIENT_ID || '0tmNTmd0Jo1dQp4AUmMBLtGiD9YpMuXzHefitcuVStc'),
      SCHEMA_PRESETS_URL: JSON.stringify(process.env.SCHEMA_PRESETS_URL || 'https://cdn.jsdelivr.net/npm/@openstreetmap/id-tagging-schema@{presets_version}/'),
      SCHEMA_PRESETS_TRANSLATIONS_URL: JSON.stringify(process.env.SCHEMA_PRESETS_TRANSLATIONS_URL || 'https://cdn.jsdelivr.net/npm/@openstreetmap/id-tagging-schema@3/dist/translations'),
      MAPILLARY_API_KEY: JSON.stringify(process.env.MAPILLARY_API_KEY || 'MLY|8573777429330689|20fde16b78284af60613276451f568bb'),
      SHOW_ALL_FIELDS: JSON.parse(process.env.SHOW_ALL_FIELDS || 'false') ? true : false,
      BACKEND_URL: JSON.stringify(process.env.BACKEND_URL || '/'),
      DEFAULT_HIGHLIGHT_COMPLETION: JSON.parse(process.env.DEFAULT_HIGHLIGHT_COMPLETION || 'false') ? true : false,
    }
  }, args))
  .then(result => {
    fs.writeFileSync('./dist/esbuild.json', JSON.stringify(result.metafile, null, 2));
  })
  .catch(() => process.exit(1));

// cdns for external data packages
const presetsCdnUrl = SCHEMA_PRESETS_URL;
const ociCdnUrl = 'https://cdn.jsdelivr.net/npm/osm-community-index@{version}/';
const wmfSitematrixCdnUrl = 'https://cdn.jsdelivr.net/npm/wmf-sitematrix@{version}/';
const nsiCdnUrl = 'https://cdn.jsdelivr.net/npm/name-suggestion-index@{version}/';

// api urls and settings
const osmApiConnections = [
  {
    url: API_ROOT,
  },
];
const taginfoApiUrl = 'https://taginfo.openstreetmap.org/api/4/';
const nominatimApiUrl = 'https://nominatim.openstreetmap.org/';

export {
  presetsCdnUrl,
  ociCdnUrl,
  wmfSitematrixCdnUrl,
  nsiCdnUrl,
  osmApiConnections,
  taginfoApiUrl,
  nominatimApiUrl,
};

FROM node:16
RUN npm install -g npm@8

USER node

RUN mkdir -p /home/node/app

WORKDIR /home/node/app

COPY --chown=node:node package.json ./
RUN npm install
COPY --chown=node:node . ./
RUN npm run all

CMD ["npm", "run", "build"]

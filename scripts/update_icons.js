const fs = require('fs');
const fetch = require('node-fetch');
const path = require('path');

const fontawesome = require('@fortawesome/fontawesome-svg-core');
const fas = require('@fortawesome/free-solid-svg-icons').fas;
const far = require('@fortawesome/free-regular-svg-icons').far;
const fab = require('@fortawesome/free-brands-svg-icons').fab;
fontawesome.library.add(fas, far, fab);

const args = process.argv.slice(2);

if (!args.length) {
  console.log("Missing arg /path/to/presets/dist/");
  process.exit(1);
}

const sources_path = args[0];

// get absolute path if the given one is relative
const sources_abs_path = (
  path.isAbsolute(sources_path) ? sources_path : path.join(
    process.cwd(),
    sources_path,
  )
);

console.log(`Parsing presets from ${sources_abs_path}`);

let faIcons = new Set([
  // list here the icons we want to use in the UI that aren't tied to other data
  'fas-i-cursor',
  'fas-lock',
  'fas-th-list',
  'fas-user-cog'
]);


[
  path.join(sources_abs_path, 'presets.min.json'),
  path.join(sources_abs_path, 'preset_categories.min.json'),
  path.join(sources_abs_path, 'fields.min.json'),
].forEach((path) => {
  const data = require(path);
  for (var key in data) {
    var datum  = data[key];
    // fontawesome icon
    if (datum.icon && /^fa[srb]-/.test(datum.icon)) {
      faIcons.add(datum.icon);
    }
  }
  // copy over only those Font Awesome icons that we need
  writeFaIcons(faIcons);
})


function writeFaIcons(faIcons) {
  Array.from(faIcons).forEach(function(key) {
    const prefix = key.substring(0, 3);   // `fas`, `far`, `fab`
    const name = key.substring(4);
    const def = fontawesome.findIconDefinition({ prefix: prefix, iconName: name });
    try {
      fs.writeFileSync(`svg/fontawesome/${key}.svg`, fontawesome.icon(def).html.toString());
    } catch (error) {
      console.error(`Error: No FontAwesome icon for ${key}`);
      throw (error);
    }
  });
}



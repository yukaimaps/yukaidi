const fs = require('fs');
const YAML = require('js-yaml');
const languageNames = require('./language_names.js');

const readCoreYaml = fs.readFileSync('data/core.yaml', 'utf8');
const readImagery = fs.readFileSync('node_modules/editor-layer-index/i18n/en.yaml', 'utf8');
const readCommunity = fs.readFileSync('node_modules/osm-community-index/i18n/en.yaml', 'utf8');
const readManualImagery = fs.readFileSync('data/manual_imagery.json', 'utf8');

return Promise.all([readCoreYaml, readImagery, readCommunity, readManualImagery])
  .then(data => {
    let core = YAML.load(data[0]);
    let imagery = YAML.load(data[1]);
    let community = YAML.load(data[2]);
    let manualImagery = JSON.parse(data[3]);

    for (let i in manualImagery) {
      let layer = manualImagery[i];
      let id = layer.id;
      for (let key in layer) {
        if (key === 'attribution') {
          for (let attrKey in layer[key]) {
            if (attrKey !== 'text') {
              delete layer[key][attrKey];
            }
          }
        } else if (['name', 'description'].indexOf(key) === -1) {
          delete layer[key];
        }
      }
      // tack on strings for additional imagery not included in the index
      imagery.en.imagery[id] = layer;
    }

    let enjson = core;
    let props = ['imagery', 'community', 'languageNames', 'scriptNames'];
    props.forEach(function(prop) {
      if (enjson.en[prop]) {
        console.error(`Error: Reserved property '${prop}' already exists in core strings`);
        process.exit(1);
      }
    });

    enjson.en.imagery = imagery.en.imagery;
    enjson.en.community = community.en;
    enjson.en.languageNames = languageNames.languageNamesInLanguageOf('en');
    enjson.en.scriptNames = languageNames.scriptNamesInLanguageOf('en');

    fs.writeFileSync('data/en.json', JSON.stringify(enjson));
  });


